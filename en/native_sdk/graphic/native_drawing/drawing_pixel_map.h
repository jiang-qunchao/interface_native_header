/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PIXEL_MAP_H
#define C_INCLUDE_DRAWING_PIXEL_MAP_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_pixel_map.h
 *
 * @brief Declares the functions related to the pixel map in the drawing module.
 *
 * File to include: native_drawing/drawing_pixel_map.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a pixel map defined by the image framework.
 * @since 12
 * @version 1.0
 */
struct NativePixelMap_;

/**
 * @brief Defines a pixel map defined by the image framework.
 * @since 12
 * @version 1.0
 */
struct OH_PixelmapNative;

/**
 * @brief Obtains the pixel map defined by this module from a pixel map defined by the image framework.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param NativePixelMap_ Pointer to a {@link NativePixelMap_} object, which is the pixel map defined by
 *        the image framework.
 * @return Returns the pointer to an {@link OH_Drawing_PixelMap} object, which is the pixel map defined by this module.
 *         If NULL is returned, the creation fails.
 *         The possible failure cause is that <b>NativePixelMap_</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_PixelMap* OH_Drawing_PixelMapGetFromNativePixelMap(NativePixelMap_*);

/**
 * @brief Obtains the pixel map defined by this module from a pixel map defined by the image framework.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_PixelmapNative Pointer to an {@link OH_PixelmapNative} object, which is the pixel map defined by
 *        the image framework.
 * @return Returns the pointer to an {@link OH_Drawing_PixelMap} object, which is the pixel map defined by this module.
 *         If NULL is returned, the creation fails.
 *         The possible failure cause is that <b>OH_PixelmapNative</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_PixelMap* OH_Drawing_PixelMapGetFromOhPixelMapNative(OH_PixelmapNative*);

/**
 * @brief Removes the relationship between a pixel map defined by this module and a pixel map defined by
 *        the image framework. The relationship is established by calling
 *        {@link OH_Drawing_PixelMapGetFromNativePixelMap} or {@link OH_Drawing_PixelMapGetFromOhPixelMapNative}.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PixelMap Pointer to an {@link OH_Drawing_PixelMap} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PixelMapDissolve(OH_Drawing_PixelMap*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
