/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Provides APIs for the camera module.
 *
 * You can use the APIs to perform operations on camera devices and streams (including offline streams),
 * and implement various callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraHost.idl
 *
 * @brief Declares the camera service management class that provides Hardware Device Interfaces (HDIs)
 * for the upper layer.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.ICameraHostCallback;
import ohos.hdi.camera.v1_0.ICameraDeviceCallback;
import ohos.hdi.camera.v1_0.ICameraDevice;

/**
 * @brief Defines the operations available for camera devices.
 *
 * You can set a callback, obtain device IDs, open a camera device, and operate the camera device.
 */
interface ICameraHost {
     /**
     * @brief Sets a callback.
     *
     * @param callbackObj Indicates the callback to set. For details, see {@link ICameraHostCallback}.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetCallback([in] ICameraHostCallback callbackObj);

     /**
     * @brief Obtains the IDs of available camera devices.
     *
     * @param cameraIds Indicates the IDs of available camera devices.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see GetCameraAbility
     *
     * @since 3.2
     * @version 1.0
     */
    GetCameraIds([out] String[] cameraIds);

     /**
     * @brief Obtains the capabilities of a camera device.
     *
     * @param cameraId Indicates the ID of the camera device, which can be obtained by calling {@link GetCameraIds}.
     * @param cameraAbility Indicates the capabilities of the camera device.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    GetCameraAbility([in] String cameraId, [out] unsigned char[] cameraAbility);

     /**
     * @brief Opens a camera device.
     *
     * By calling this function, you can obtain an <b>ICameraDevice</b> instance and
     * operate the camera device matching the instance.
     *
     * @param cameraId Indicates the ID of the camera device, which can be obtained by calling {@link GetCameraIds}.
     * @param callbackObj Indicates the callback related to the camera. For details, see {@link ICameraDeviceCallback}.
     * @param device Indicates the <b>ICameraDevice</b> instance corresponding to the ID of the camera device.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    OpenCamera([in] String cameraId, [in] ICameraDeviceCallback callbackObj, [out] ICameraDevice device);

     /**
     * @brief Turns on or off the flash.
     *
     * This function can be used only by the caller who has opened the camera device specified by <b>cameraId</b>.
     *
     * @param cameraId Indicates the ID of the camera device.
     * @param isEnable Specifies whether to turn on or off the flash. The value <b>true</b> means to turn on the flash,
     * and <b>false</b> means the opposite.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    SetFlashlight([in] String cameraId, [in] boolean isEnable);
}
/** @} */
