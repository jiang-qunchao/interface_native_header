/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Distributed Audio
 * @{
 *
 * @brief Distributed Audio
 *
 * Distributed Audio模块包括对分布式音频设备的操作、流的操作和各种回调等。
 * 通过IDAudioCallback和IDAudioManager接口，与Source SA通信交互，实现分布式功能。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file Types.idl
 *
 * @brief Audio模块接口定义中使用的数据类型。
 *
 * Audio模块接口定义中使用的数据类型，包括音频播放模式、采样属性、事件类型、数据类型、时间戳等
 *
 * 模块包路径：ohos.hdi.distributed_audio.audioext.v1_0
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.distributed_audio.audioext.v1_0;

/**
 * @brief 音频端口播放模式。
 *
 * @since 4.1
 * @version 1.0
 */
enum PortOperationMode {
    NORMAL_MODE = 0,  /**< 正常模式。**/
    MMAP_MODE = 1,    /**< 低时延模式。**/
};

/**
 * @brief 音频采样属性。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioParameter {
    unsigned int format;                   /**< 音频采样频率。**/
    unsigned int channelCount;             /**< 音频通道数目，如单通道为1、立体声为2。**/
    unsigned int sampleRate;               /**< 音频采样频率。**/
    unsigned int period;                   /**< 音频采样周期，单位赫兹。**/
    unsigned int frameSize;                /**< 音频数据的帧大小。**/
    unsigned int streamUsage;              /**< 音频流使用情况。**/
    enum PortOperationMode renderFlags;    /**< 音频播放标志，用于标志播放模式。**/
    enum PortOperationMode capturerFlags;  /**< 音频录音标志，用于标志播放模式。**/
    String ext;                            /**< 参数描述。**/
};

/**
 * @brief 音频数据类型。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioData {
    struct AudioParameter param;  /**< 音频参数。**/
    byte[] data;                  /**< 数据内容。**/
};

/**
 * @brief 音频事件类型。
 *
 * @since 4.1
 * @version 1.0
 */
struct DAudioEvent {
    int type;        /**< 事件类型。**/
    String content;  /**< 事件内容。**/         
};

/**
 * @brief 音频时间戳。
 *
 * @since 4.1
 * @version 1.0
 */
struct CurrentTime {
    long tvSec;   /**< tvSec时间，单位：秒。**/
    long tvNSec;  /**< tvNSec时间，单位：纳秒。**/
};
/** @} */