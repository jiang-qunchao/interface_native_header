/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiGnss
 * @{
 *
 * @brief 定义GNSS模块的接口。
 *
 * 上层GNSS服务可以获取GNSS驱动对象或代理，然后调用该对象或代理提供的API来访问GNSS设备，
 * 从而实现启动GNSS芯片，启动导航，设置GNSS工作模式，注入参考信息，获取定位结果，获取nmea，
 * 获取卫星状态信息，批量获取缓存位置信息等。
 *
 * @since 3.2
 */

/**
 * @file IGnssInterface.idl
 *
 * @brief 声明GNSS模块提供的接口函数，包括启动GNSS芯片、启动导航、设置GNSS工作模式、注入参考信息、
 * 删除辅助数据、注入PGNSS数据、获取GNSS缓存位置个数、获取所有缓存位置。
 *
 * 模块包路径：ohos.hdi.location.gnss.v1_0
 *
 * 引用：
 * - ohos.hdi.location.gnss.v1_0.IGnssCallback
 * - ohos.hdi.location.gnss.v1_0.GnssTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.gnss.v1_0;

import ohos.hdi.location.gnss.v1_0.IGnssCallback;
import ohos.hdi.location.gnss.v1_0.GnssTypes;

/**
 * @brief 声明GNSS模块提供的接口函数，包括启动GNSS芯片、启动导航、设置GNSS工作模式、注入参考信息、
 * 删除辅助数据、注入PGNSS数据、获取GNSS缓存位置个数、获取所有缓存位置。
 *
 * @since 3.2
 */
interface IGnssInterface {
    /**
     * @brief 设置GNSS配置参数。
     *
     * @param para 表示GNSS配置参数。包含基础的GNSS配置和GNSS缓存位置功能配置参数。详情参考{@link GnssConfigPara}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetGnssConfigPara([in] struct GnssConfigPara para);

    /**
     * @brief 使能GNSS功能，并设置回调函数。
     *
     * @param callback 表示GNSS回调函数。GNSS驱动通过此回调函数上报定位结果和卫星状态信息等。
     * 详情参考{@link IGnssCallback}.
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    EnableGnss([in] IGnssCallback callbackObj);

    /**
     * @brief 去使能GNSS功能。
     *
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DisableGnss();

    /**
     * @brief 启动导航功能。
     *
     * @param type 表示GNSS启动类型，该参数是为了区分正常的GNSS定位功能和GNSS缓存功能。
     * 详情参考{@link GnssStartType}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    StartGnss([in] enum GnssStartType type);

    /**
     * @brief 停止导航功能。
     *
     * @param type 表示GNSS启动类型，该参数为了区分正常的GNSS定位功能和GNSS缓存功能。
     * 详情参考{@link GnssStartType}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    StopGnss([in] enum GnssStartType type);

    /**
     * @brief 注入GNSS参考信息。
     *
     * @param refInfo 表示GNSS参考信息，包含参考时间和参考位置。详情参考{@link GnssRefInfo}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetGnssReferenceInfo([in] struct GnssRefInfo refInfo);

    /**
     * @brief 删除指定的辅助数据。
     *
     * @param data 表示辅助数据类型。详情参考{@link GnssAuxiliaryData}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeleteAuxiliaryData([in] enum GnssAuxiliaryData data);

    /**
     * @brief 注入PGNSS数据。
     *
     * @param data 表示PGNSS数据。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetPredictGnssData([in] String data);

    /**
     * @brief 获取GNSS缓存位置个数。
     *
     * @param size 表示GNSS缓存位置个数。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetCachedGnssLocationsSize([out] int size);

    /**
     * @brief 请求一次性获取GNSS缓存中的所有位置信息，并清空缓存buffer，缓存位置通过回调上报。
     *
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetCachedGnssLocations();
}
/** @} */