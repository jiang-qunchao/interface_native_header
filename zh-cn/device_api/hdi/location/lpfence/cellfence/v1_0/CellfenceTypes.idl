/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellfence
 * @{
 *
 * @brief 为低功耗围栏服务提供基站围栏的API。
 *
 * 本模块接口提供添加基站围栏、删除基站围栏和获取基站围栏使用信息的功能。
 *
 * 应用场景：判断用户设备是否达到某个较大范围的位置区域，从而进行一些后续服务，如景区服务介绍等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file CellfenceTypes.idl
 *
 * @brief 定义基站围栏使用的数据类型。
 *
 * 模块包路径：ohos.hdi.location.lpfence.cellfence.v1_0
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.cellfence.v1_0;

/**
 * @brief 枚举设备与基站围栏的位置关系。
 *
 * @since 4.0
 */
enum CellfenceTransition {
    /** 设备在基站围栏范围内。 */
    FENCE_ENTER = 0,
    /** 设备不在基站围栏范围内。 */
    FENCE_EXIT = 3,
};

/**
 * @brief 定义基站基本信息的数据结构。
 *
 * @since 4.0
 */
struct CellfenceInfo {
    /** 基站号 */
    unsigned int lac;
    /** 小区号 */
    unsigned long cid;
};

/**
 * @brief 定义添加基站围栏的数据结构。
 *
 * @since 4.0
 */
struct CellfenceRequest {
    /** 基站围栏的ID号，用于标识某个基站围栏，不可重复添加相同ID号的围栏。 */
    int cellfenceId;
    /** 基站围栏信息，详见{@link CellfenceInfo}。 */
    struct CellfenceInfo[] cellInfo;
};

/**
 * @brief 定义设备与基站围栏状态关系的数据结构。
 *
 * @since 4.0
 */
struct CellfenceStatus {
    /** 基站围栏的ID号，用于标识某个基站围栏。 */
    int cellfenceId;
    /** 设备与该基站围栏的位置关系。详见{@link CellfenceTransition}。 */
    unsigned short status;
};

/**
 * @brief 定义基站围栏使用信息的数据结构。
 *
 * @since 4.0
 */
struct CellfenceSize {
    /** 设备支持的最大基站围栏个数。 */
    unsigned int maxNum;
    /** 当前设备已添加的基站围栏个数。 */
    unsigned int usedNum;
};
/** @} */