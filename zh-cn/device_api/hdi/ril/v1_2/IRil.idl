/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Ril
 * @{
 *
 * @brief Ril模块接口定义。
 *
 * Ril模块为上层电话服务提供相关调用接口，涉及电话、短信、彩信、网络搜索、SIM卡等功能接口及各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IRil.idl
 *
 * @brief Ril模块的请求接口。
 *
 * 模块包路径：ohos.hdi.ril.v1_2
 *
 * 引用：
 * - ohos.hdi.ril.v1_2.IRilCallback
 * - ohos.hdi.ril.v1_1.IRil
 * - ohos.hdi.ril.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */


package ohos.hdi.ril.v1_2;

import ohos.hdi.ril.v1_2.IRilCallback;
import ohos.hdi.ril.v1_1.IRil;
import ohos.hdi.ril.v1_2.Types;

/**
 * @brief Ril模块的请求接口。
 *
 * 请求接口包括打电话、发短信彩信、激活SIM卡、上网等。
 *
 * @since 4.1
 * @version 1.2
 */
interface IRil extends ohos.hdi.ril.v1_1.IRil {
     /**
      * @brief 设置IRil回调接口，回调函数参考{@link IRilCallback}。
      *
      * @param rilCallback 要设置的回调函数。
      *
      * @return 0 表示执行成功。
      * @return 非零值 表示操作失败。
      *
      * @since 4.1
      * @version 1.2
      */
    [oneway] SetCallback1_2([in] IRilCallback rilCallback);

     /**
      * @brief 下发随卡信息接口。
      *
      * @param slotId 表示卡槽ID。
      * @param serialId 表示请求的序列化ID。
      * @param ncfgOperatorInfo 要下发的随卡信息，参考{@link NcfgOperatorInfo}。
      *
      * @return 0 表示执行成功。
      * @return 非零值 表示操作失败。
      *
      * @since 4.1
      * @version 1.2
      */
    [oneway] SendSimMatchedOperatorInfo([in] int slotId, [in] int serialId,
        [in] struct NcfgOperatorInfo ncfgOperatorInfo);

     /**
      * @brief 清除所有数据连接。
      *
      * @param slotId 表示卡槽ID。
      * @param serialId 表示请求的序列化ID。
      *
      * @return 0 表示执行成功。
      * @return 非零值 表示操作失败。
      *
      * @since 4.1
      * @version 1.2
      */
    [oneway] CleanAllConnections([in] int slotId, [in] int serialId);
}
/** @} */
