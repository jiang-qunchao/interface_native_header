/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceEngine
 * @{
 *
 * @brief IntelligentVoiceEngine模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上统一接口获取如下能力：创建销毁唤醒算法引擎、启动停止唤醒算法引擎、写语音数据、读文件、回调函数注册等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IntellVoiceEngineTypes.idl
 *
 * @brief IntelligentVoiceEngine模块接口定义中使用的数据类型，包括引擎适配器类型、数据类型、回调消息类型、回调消息错误码、引擎适配器描述、回调事件信息等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.engine.v1_0
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.intelligent_voice.engine.v1_0;

/**
 * @brief 智能语音引擎适配器类型。
 * 
 * @since 4.0
 * @version 1.0
 */
enum IntellVoiceEngineAdapterType {
    /** 注册引擎适配器 */
    ENROLL_ADAPTER_TYPE = 0,
    /** 唤醒引擎适配器 */
    WAKEUP_ADAPTER_TYPE = 1,
    /** 静默升级引擎适配器 */
    UPDATE_ADAPTER_TYPE = 2,
    /** 无效引擎适配器 */
    ADAPTER_TYPE_BUT,
};

/**
 * @brief 数据类型。
 *
 * 上层服务读取的数据类型。
 *
 * @since 4.0
 * @version 1.0
 */
enum ContentType {
    /** DSP模型文件 */
    DSP_MODLE = 0,
    /** 无效数据类型 */
    CONTENT_TYPE_BUT,
};

/**
 * @brief 回调消息类型。
 *
 * 通知上层服务的消息类型。
 *
 * @since 4.0
 * @version 1.0
 */
enum IntellVoiceEngineMessageType {
    /** 无效消息类型 */
    INTELL_VOICE_ENGINE_MSG_NONE = 0,
    /** 初始化完成消息 */
    INTELL_VOICE_ENGINE_MSG_INIT_DONE = 1,
    /** 注册完成消息 */
    INTELL_VOICE_ENGINE_MSG_ENROLL_COMPLETE = 2,
    /** 确认注册完成消息 */
    INTELL_VOICE_ENGINE_MSG_COMMIT_ENROLL_COMPLETE = 3,
    /** 唤醒识别消息 */
    INTELL_VOICE_ENGINE_MSG_RECOGNIZE_COMPLETE = 4,
};

/**
 * @brief 回调消息错误码。
 *
 * 通知上层服务的消息错误码。
 *
 * @since 4.0
 * @version 1.0
 */
enum IntellVoiceEngineErrors {
    /** 成功 */
    INTELL_VOICE_ENGINE_OK = 0,
    /** 错误码偏移 */
    INTELL_VOICE_ENGINE_ERROR_OFFSET = -100,
    /** 无效参数 */
    INTELL_VOICE_ENGINE_INVALID_PARAMS = -101,
    /** 初始化失败 */
    INTELL_VOICE_ENGINE_INIT_FAILED = -102,
    /** 注册失败 */
    INTELL_VOICE_ENGINE_ENROLL_FAILED = -103,
    /** 确认注册失败 */
    INTELL_VOICE_ENGINE_COMMIT_ENROLL_FAILED = -104,
    /** 唤醒失败 */
    INTELL_VOICE_ENGINE_WAKEUP_FAILED = -105,
};

/**
 * @brief 智能语音引擎适配器描述符。
 *
 * 一个智能语音引擎适配器（adapter）对应一个智能语音引擎算法实例。
 *
 * @since 4.0
 * @version 1.0
 *
 */
struct IntellVoiceEngineAdapterDescriptor {
    /** 智能语音引擎适配器类型。 */
    enum IntellVoiceEngineAdapterType adapterType;
};

/**
 * @brief 智能语音引擎适配器信息。
 *
 * @since 4.0
 * @version 1.0
 *
 */
struct IntellVoiceEngineAdapterInfo {
    /** 唤醒词。 */
    String wakeupPhrase;
    /** 最小语音数据量。 */
    int minBufSize;
    /** 通道数。 */
    int sampleChannels;
    /** 位宽。 */
    int bitsPerSample;
    /** 采样率。 */
    int sampleRate;
};

/**
 * @brief 智能语音引擎启动信息。
 *
 * @since 4.0
 * @version 1.0
 *
 */
struct StartInfo {
    /** 是否最后一次启动引擎。 */
    boolean isLast;
};

/**
 * @brief 智能语音引擎回调事件信息。
 *
 * @since 4.0
 * @version 1.0
 *
 */
struct IntellVoiceEngineCallBackEvent {
    /** 回调消息类型。 */
    enum IntellVoiceEngineMessageType msgId;
    /** 回调消息结果。 */
    enum IntellVoiceEngineErrors result;
    /** 回调消息内容。 */
    String info;
};
/** @} */