/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MediaAssetManager
 * @{
 *
 * @brief 提供媒体库资源请求能力的API。
 *
 * @since 12
 */

/**
 * @file media_asset_manager.h
 *
 * @brief 定义媒体资产管理器的接口。
 *
 * 使用由媒体资产管理器提供的C API来请求媒体库资源。
 *
 * @Syscap SystemCapability.FileManagement.PhotoAccessHelper.Core
 * @library libmedia_asset_manager.so
 * @since 12
 */

#ifndef MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_MANAGER_H
#define MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_MANAGER_H

#include "media_asset_base_capi.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个媒体资产管理器。
 *
 * @return 返回一个指向OH_MediaAssetManager实例的指针。
 * @since 12
*/
OH_MediaAssetManager* OH_MediaAssetManager_Create(void);

/**
 * @brief 请求具有目标路径的图像资源。
 *
 * @permission ohos.permission.READ_IMAGEVIDEO
 * @param manager 指向OH_MediaAssetManager实例的指针。
 * @param uri 请求的图像资源的uri。
 * @param requestOptions 请求策略模式配置项。
 * @param destPath 请求资源的目标地址。
 * @param callback 媒体资源处理器，当所请求的媒体资源准备完成时会触发回调。
 * @return 返回请求Id。
 * @since 12
*/
MediaLibrary_RequestId OH_MediaAssetManager_RequestImageForPath(OH_MediaAssetManager* manager, const char* uri,
    MediaLibrary_RequestOptions requestOptions, const char* destPath, OH_MediaLibrary_OnDataPrepared callback);

/**
 * @brief 请求具有目标路径的视频资源。
 *
 * @permission ohos.permission.READ_IMAGEVIDEO
 * @param manager 指向OH_MediaAssetManager实例的指针。
 * @param uri 请求的视频资源的uri。
 * @param requestOptions 请求策略模式配置项。
 * @param destPath 请求资源的目标地址。
 * @param callback 媒体资源处理器，当所请求的媒体资源准备完成时会触发回调。
 * @return 返回请求Id。
 * @since 12
*/
MediaLibrary_RequestId OH_MediaAssetManager_RequestVideoForPath(OH_MediaAssetManager* manager, const char* uri,
    MediaLibrary_RequestOptions requestOptions, const char* destPath, OH_MediaLibrary_OnDataPrepared callback);

/**
 * @brief 通过请求Id取消请求。
 *
 * @permission ohos.permission.READ_IMAGEVIDEO
 * @param manager 指向OH_MediaAssetManager实例的指针。
 * @param requestId 待取消的请求Id。
 * @return 如果请求成功取消，则返回true；否则返回false。
 * @since 12
*/
bool OH_MediaAssetManager_CancelRequest(OH_MediaAssetManager* manager, const MediaLibrary_RequestId requestId);

#ifdef __cplusplus
}
#endif
#endif // MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_MANAGER_H