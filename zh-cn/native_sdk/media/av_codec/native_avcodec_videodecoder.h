/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoDecoder
 * @{
 * 
 * @brief VideoDecoder模块提供用于视频解码的函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @since 9
 * @version 1.0
 */


/**
 * @file native_avcodec_videodecoder.h
 * 
 * @brief 声明用于视频解码的Native API。
 * 
 * @library libnative_media_vdec.so
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVCODEC_VIDEODECODER_H
#define NATIVE_AVCODEC_VIDEODECODER_H

#include <stdint.h>
#include <stdio.h>
#include "native_avcodec_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 从MIME类型创建视频解码器实例，大多数情况下建议使用。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param mime MIME类型描述字符串，请参阅{@link AVCODEC_MIME_TYPE}。
 * @return 返回一个指向视频解码实例的指针。
 * 当传入的解码器类型不支持或者内存资源耗尽时，返回空指针。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoDecoder_CreateByMime(const char *mime);

/**
 * @brief 通过视频解码器名称创建视频解码器实例。使用此接口的前提是知道解码器的确切名称。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param name 视频解码器名称。
 * @return 返回指向视频解码实例的指针。
 * 当输入的解码器名称不支持或者内存资源耗尽时，返回空指针。
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoDecoder_CreateByName(const char *name);

/**
 * @brief 清理解码器内部资源，销毁解码器实例。
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当解码器实例已经销毁，返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Destroy(OH_AVCodec *codec);

/**
 * @brief 设置异步回调函数，以便您的应用程序可以响应视频解码器生成的事件。在调用Prepare之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param callback 所有回调函数的集合，请参阅{@link OH_AVCodecAsyncCallback}。
 * @param userData 用户特定数据。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_RegisterCallback
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetCallback(OH_AVCodec *codec, OH_AVCodecAsyncCallback callback, void *userData);

/**
 * @brief 注册异步回调函数，以便您的应用程序可以响应视频解码器生成的事件。在调用Prepare之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码器实例的指针。
 * @param callback 所有回调函数的集合，请参见{@link OH_AVCodecCallback}。
 * @param userData 用户特定数据。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_RegisterCallback(OH_AVCodec *codec, OH_AVCodecCallback callback, void *userData);

/**
 * @brief 指定输出表面以提供视频解码输出，必须在调用Prepare之前调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param window 指向OHNativeWindow实例的指针，请参阅{@link OHNativeWindow}。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 本接口仅支持在surface模式下调用，如果在buffer模式调用，返回{@link AV_ERR_OPERATE_NOT_PERMIT}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetSurface(OH_AVCodec *codec, OHNativeWindow *window);

/**
 * @brief 配置视频解码器，通常需要配置解码视频轨迹的描述信息，这些信息可以从OH_AVSource中提取。在调用准备之前，必须调用此接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param format 指向OH_AVFormat的指针，用于给出要解码的视频轨道的描述。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，或输入format参数不支持，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 本接口必须在Prepare接口前调用，如果在其他状态时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Configure(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 要准备解码器的内部资源，在调用该接口之前，必须调用Configure接口。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Prepare(OH_AVCodec *codec);

/**
 * @brief 启动解码器，准备成功后必须调用此接口。成功启动后，解码器将开始报告注册的回调事件。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Start(OH_AVCodec *codec);

/**
 * @brief 停止解码器。停止后，可以通过Start重新进入Started状态。
 * 
 * 但需要注意的是，如果编解码器特定数据以前已输入到解码器，则需要再次输入。
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Stop(OH_AVCodec *codec);

/**
 * @brief 清除解码器中缓存的输入和输出数据。
 * 调用此接口后，以前通过异步回调上报的所有缓冲区索引都将失效，请确保不要访问这些索引对应的缓冲区。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Flush(OH_AVCodec *codec);

/**
 * @brief 重置解码器。如果要继续解码，需要再次调用Configure接口配置解码器实例。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_Reset(OH_AVCodec *codec);

/**
 * @brief 获取解码器输出数据的描述信息，请参阅{@link OH_AVFormat}。
 * 需要注意的是，返回值指向的OH_AVFormat实例的生命周期需要调用者手动释放。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @return 返回指向OH_AVFormat实例的指针。
 * 当输入的codec指针非解码器实例，或者为空指针，则返回空指针。
 * @since 9
 * @version 1.0
 */
OH_AVFormat *OH_VideoDecoder_GetOutputDescription(OH_AVCodec *codec);

/**
 * @brief 设置解码器的动态参数。注意，该接口只能在解码器启动后调用。同时，参数配置错误可能会导致解码失败。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param format 指向OH_AVFormat实例的指针。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，或输入format参数不支持，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_SetParameter(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief 将填充数据的输入缓冲区提交给视频解码器。
 * 输入回调将报告可用的输入缓冲区和相应的索引值，请参阅{@OH_AVCodecOnNeedInputData}。
 * 一旦具有指定索引的缓冲区提交到视频解码器，则无法再次访问缓冲区，直到再次收到输入回调，报告具有相同索引的缓冲区可用。
 * 此外，对于某些解码器，需要在开始时向解码器输入编解码特定数据，以初始化解码器的解码过程，如H264格式的PPS/SPS数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输入Buffer对应的索引值。
 * @param attr 	描述缓冲区中包含的数据的信息。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index值应该由{@link OH_AVCodecOnNeedInputData}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_PushInputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_PushInputData(OH_AVCodec *codec, uint32_t index, OH_AVCodecBufferAttr attr);

/**
 * @brief 将处理后的输出Buffer返回给解码器，并通知解码器完成在输出表面上渲染Buffer中包含的解码数据。
 * 如果之前没有配置输出表面，则调用此接口仅将指定索引对应的输出缓冲区返回给解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出Buffer对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index应该由{@link OH_AVCodecOnNewOutputData}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_RenderOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_RenderOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的输出缓冲区返回到解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出Buffer对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}。
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index应该由{@link OH_AVCodecOnNewOutputData}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @deprecated since 11
 * @useinstead OH_VideoDecoder_FreeOutputBuffer
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoDecoder_FreeOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将填充数据的输入缓冲区提交给视频解码器。
 * 输入回调将报告可用的输入缓冲区和相应的索引值，请参阅{@OH_AVCodecOnNeedInputBuffer}。
 * 一旦具有指定索引的缓冲区提交到视频解码器，则无法再次访问缓冲区，直到再次收到输入回调，报告具有相同索引的缓冲区可用。
 * 此外，对于某些解码器，需要在开始时向解码器输入编解码特定数据，以初始化解码器的解码过程，如H264格式的PPS/SPS数据。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输入Buffer对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * index应该由{@link OH_AVCodecOnNeedInputBuffer}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_PushInputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的输出缓冲返回给解码器，并通知解码器完成在输出表面上渲染，输出缓冲包含解码数据。
 * 如果之前没有配置输出表面，则调用此接口仅将指定索引对应的输出缓冲区返回给解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param index 输出Buffer对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 缓冲区索引值应该由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_RenderOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 将处理后的输出缓冲区返回到解码器。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针
 * @param index 输出Buffer对应的索引值。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 缓冲区索引值应该由{@link OH_AVCodecOnNewOutputBuffer}给出。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * 当解码器状态不支持调用本接口时调用，返回{@link AV_ERR_INVALID_STATE}。
 
 * @since 11
 */
OH_AVErrCode OH_VideoDecoder_FreeOutputBuffer(OH_AVCodec *codec, uint32_t index);

/**
 * @brief 检查当前解码实例是否有效。
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param isValid 输出参数。指向布尔实例的指针，如果解码器实例有效，则为true，如果解码器实例无效，则为false。
 * @return 如果执行成功，则返回AV_ERR_OK，否则返回特定错误代码，请参阅{@link OH_AVErrCode}
 * 当输入的解码器实例已经销毁，调用本接口会返回{@link AV_ERR_NO_MEMORY}。
 * 当输入的codec指针非解码器实例，或者为空指针，返回{@link AV_ERR_INVALID_VAL}。
 * 未知错误会返回{@link AV_ERR_UNKNOWN}。
 * 当服务状态已经消亡，返回{@link AV_ERR_SERVICE_DIED}。
 * @since 10
 */
OH_AVErrCode OH_VideoDecoder_IsValid(OH_AVCodec *codec, bool *isValid);

/**
 * @brief 设置解密配置。在调用准备之前，可选择调用此接口。
 *
 * @syscap SystemCapability.Multimedia.Media.VideoDecoder
 * @param codec 指向视频解码实例的指针。
 * @param mediaKeySession 指向带有解密功能的DRM会话实例的指针，请参阅{@link MediaKeySession}。
 * @param secureVideoPath 安全视频通路。指定安全视频通路为true，否则为false。
 * @return 返回函数结果代码{@link OH_AVErrCode}：
 *         AV_ERR_OK：操作成功。
 *         AV_ERR_OPERATE_NOT_PERMIT：编解码器服务或媒体密钥会话服务处于错误状态。
 *         AV_ERR_INVALID_VAL：编解码器实例为nullptr或无效，或者mediaKeySession为nullptr或无效。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_VideoDecoder_SetDecryptionConfig(OH_AVCodec *codec, MediaKeySession *mediaKeySession,
    bool secureVideoPath);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_VIDEODECODER_H
/** @} */